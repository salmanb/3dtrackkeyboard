//
//  GuestureStateChange.m
//  iOSKeyboardTemplateContainer
//
//  Created by Salman.Ahmed on 6/18/16.
//  Copyright © 2016 Jeff Blagg. All rights reserved.
//

#import "GuestureStateChange.h"

@implementation GuestureStateChange
- (UIGestureRecognizerState) setstate{
    return UIGestureRecognizerStateEnded;
}
@end
