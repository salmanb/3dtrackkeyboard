//
//  DragGestureRecognizer.m
//  KeyboardTrackpad
//
//  Created by Munib Ahmed Siddiqui on 6/19/16.
//  Copyright © 2016 Jeff Blagg. All rights reserved.
//

#import "DragGestureRecognizer.h"

@implementation DragGestureRecognizer
@synthesize firstTouch, lastTouch;


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [super touchesMoved:touches withEvent:event];
    
    if ([self.delegate respondsToSelector:@selector(gestureRecognizer:movedWithTouches:andEvent:)]) {
        [(id)self.delegate gestureRecognizer:self movedWithTouches:touches andEvent:event];
    }
    
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self setFirstTouch:[touches anyObject]];
    [super touchesBegan:touches withEvent:event];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self setLastTouch:[touches anyObject]];
    [super touchesEnded:touches withEvent:event];
}

@end
