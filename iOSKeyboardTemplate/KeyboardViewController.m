//
//  KeyboardViewController.m
//  iOSKeyboardTemplate
//
//  Copyright (c) 2014 BJH Studios. All rights reserved.
//  questions or comments contact jeff@bjhstudios.com

#import "KeyboardViewController.h"
#import "GuestureStateChange.h"



#define  THRESHOLD_SPEED 3000

@interface KeyboardViewController () {
    
    int _shiftStatus; //0 = off, 1 = on, 2 = caps lock
    
}
//keyboard rows
@property (nonatomic, weak) IBOutlet UIView *numbersRow1View;
@property (nonatomic, weak) IBOutlet UIView *numbersRow2View;
@property (nonatomic, weak) IBOutlet UIView *symbolsRow1View;
@property (nonatomic, weak) IBOutlet UIView *symbolsRow2View;
@property (nonatomic, weak) IBOutlet UIView *numbersSymbolsRow3View;
@property (weak, nonatomic) IBOutlet UIView *alphabatRow1View;
@property (weak, nonatomic) IBOutlet UIView *alphabatRow2View;
@property (weak, nonatomic) IBOutlet UIView *alphabatRow3View;

//keys
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *letterButtonsArray;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *numbersButtonsArray;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *allButtonsArray;


@property (nonatomic, weak) IBOutlet UIButton *switchModeRow3Button;
@property (nonatomic, weak) IBOutlet UIButton *switchModeRow4Button;
@property (nonatomic, weak) IBOutlet UIButton *shiftButton;
@property (nonatomic, weak) IBOutlet UIButton *spaceButton;
@property (strong, nonatomic) IBOutlet UIView *curserView;
@property (weak, nonatomic) IBOutlet UIImageView *allowKeyboardToFullAccess;
@property (weak, nonatomic) IBOutlet UIImageView *keyboardBackgroundImageView;

@end

@implementation KeyboardViewController
@synthesize soundFileURLRef;
@synthesize soundFileObject;

- (void)updateViewConstraints {
    [super updateViewConstraints];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    right  = 0;
    left = 0;
    [self initializeKeyboard];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if([self isOpenAccessGranted]){
        [self.allowKeyboardToFullAccess setHidden:YES];
    }else{
        [self.allowKeyboardToFullAccess setHidden:NO];
    }
    
    // set keyboard skin
    [self setButtonSkin];
}
- (void)viewDidAppear:(BOOL)animated{
    
    
    //NSLog(@"%d",[self isOpenAccessGranted]);
    [self addSound];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark Animate buttons
-(void)animateButton:(UIButton *)btn{
    //    [UIView animateWithDuration:0.3/1.5 animations:^{
    //        btn.transform = CGAffineTransformScale(CGAffineTransformIdentity, 2.4, 2.4); // scales up the view of button
    //    } completion:^(BOOL finished) {
    //        [UIView animateWithDuration:0.3/3.6 animations:^{
    //            btn.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7);// scales down the view of button
    //        } completion:^(BOOL finished) {
    //            [UIView animateWithDuration:0.3/3.6 animations:^{
    //                btn.transform = CGAffineTransformIdentity; // at the end sets the original identity of the button
    //            }];
    //        }];
    //    }];
    
    //    // instantaneously make the image view small (scaled to 1% of its actual size)
    //    btn.transform = CGAffineTransformMakeScale(0.4, 0.4);
    //    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    //        // animate it to the identity transform (100% scale)
    //        btn.transform = CGAffineTransformIdentity;
    //    } completion:^(BOOL finished){
    //        // if you want to do something once the animation finishes, put it here
    //    }];
    
    CGRect originalFrame = btn.frame;
    [UIView animateWithDuration:0.3/3.0 animations:^{
        btn.frame = CGRectOffset(btn.frame, 0, -20);
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/3.6 animations:^{
            //btn.transform = CGAffineTransformIdentity; // at the end sets the original identity of the butto
            btn.frame = originalFrame;
        }];
    }];
}
#pragma mark Reskin Keyboard
- (void) setButtonSkin{
    [self.keyboardBackgroundImageView setBackgroundColor:[UIColor colorWithRed:183.0/255.0f green:187.0/255.0f blue:193.0/255.0f alpha:1.0]];
    CGFloat fontSizeValue = 22.f;
    NSString* fontName = @"Helvetica";
    for (UIButton* letterButton in self.allButtonsArray) {
        [letterButton.titleLabel setFont:[UIFont fontWithName:fontName size:fontSizeValue]];
        letterButton.titleLabel.adjustsFontSizeToFitWidth = NO;
        [letterButton setTitle:letterButton.titleLabel.text.lowercaseString forState:UIControlStateNormal];
        [letterButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        letterButton.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:251.0/255.0 blue:251.0/255.0 alpha:1.0];
        [self setButtonProperty:letterButton];
    }
    
}
- (void) setButtonProperty: (UIButton*) letterButton{
    letterButton.layer.cornerRadius = 6.0f;
    letterButton.layer.masksToBounds = NO;
    letterButton.layer.borderWidth = 1.0f;
    letterButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    letterButton.layer.shadowColor = [UIColor grayColor].CGColor;
    letterButton.layer.shadowOpacity = 1.0f;
    letterButton.layer.shadowRadius = 0;
    letterButton.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
}
#pragma mark Other Button Action

- (void) addSound{
    // Create the URL for the source audio file. The URLForResource:withExtension: method is
    //    new in iOS 4.0.
    NSURL *tapSound   = [[NSBundle mainBundle] URLForResource: @"tap"
                                                withExtension: @"aif"];
    
    // Store the URL as a CFURLRef instance
    self.soundFileURLRef = (__bridge CFURLRef) tapSound;
    NSLog(@"%s   %u %@",__PRETTY_FUNCTION__,(unsigned int)self.soundFileObject,self.soundFileURLRef);
    // Create a system sound object representing the sound file.
    AudioServicesCreateSystemSoundID (
                                      
                                      soundFileURLRef,
                                      &soundFileObject
                                      );
}
// Respond to a tap on the System Sound button.
- (void) playSystemSound: (id) sender {
    
    AudioServicesPlaySystemSound (soundFileObject);
}


// Respond to a tap on the Alert Sound button.
- (void) playAlertSound: (id) sender {
    
    AudioServicesPlayAlertSound (soundFileObject);
}


// Respond to a tap on the Vibrate button. In the Simulator and on devices with no
//    vibration element, this method does nothing.
- (void) vibrate: (id) sender {
    
    AudioServicesPlaySystemSound (0x450);
}

-(BOOL)isOpenAccessGranted{
    return [UIPasteboard generalPasteboard];
}
#pragma mark Long touch
- (void) addGuestureOnKeyboard{
    [self addLongTouchGuestureView:self.alphabatRow1View];
    [self addLongTouchGuestureView:self.alphabatRow2View];
    [self addLongTouchGuestureView:self.alphabatRow3View];
    [self addLongTouchGuestureView:self.numbersRow1View];
    [self addLongTouchGuestureView:self.numbersRow2View];
    [self addLongTouchGuestureView:self.numbersSymbolsRow3View];
    [self addLongTouchGuestureView:self.symbolsRow1View];
    [self addLongTouchGuestureView:self.symbolsRow2View];
    
    
    for (UIButton* letterButton in self.letterButtonsArray) {
        [self addLongTouchGuestureView:letterButton];
        
    }
    for (UIButton* letterButton in self.numbersButtonsArray) {
        [self addLongTouchGuestureView:letterButton];
        
    }
}
- (void)addLongTouchGuestureView:(id) sender{
    newRecognizer = [[DragGestureRecognizer alloc]initWithTarget:self
                                                          action:@selector(longPressedView:)];
    [newRecognizer setDelegate:self];
    [sender addGestureRecognizer:newRecognizer];
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    return YES;
}
- (void)longPressedView:(UILongPressGestureRecognizer *)sender {
    NSLog(@" setHidden:NO %s",__PRETTY_FUNCTION__);
    
    
    
}

- (void) gestureRecognizer:(UIGestureRecognizer *)gr movedWithTouches:(NSSet*)touches andEvent:(UIEvent *)event{
    
    if (gr.state == UIGestureRecognizerStateBegan) {
        NSLog(@"UIGestureRecognizerStateBegan");
        //Do Whatever You want on End of Gesture
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           //AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
                           AudioServicesPlaySystemSound (1103);
                           
                       });
        [self.curserView setHidden:YES];
        LongGesturePress = true;
        previousTimestamp = event.timestamp;
    }
    else
        if (gr.state == UIGestureRecognizerStateEnded) {
            NSLog(@"UIGestureRecognizerStateEnded");
            //Do Whatever You want on End of Gesture
            
        }
        else if (gr.state == UIGestureRecognizerStateCancelled){
            NSLog(@"UIGestureRecognizerStateCancelled.");
            //Do Whatever You want on Began of Gesture
            
        }else if (gr.state == UIGestureRecognizerStateFailed){
            NSLog(@"UIGestureRecognizerStateFailed.");
            
            //Do Whatever You want on Began of Gesture
            
        }
        else if (gr.state == UIGestureRecognizerStatePossible){
            //Do Whatever You want on Began of Gesture
            NSLog(@"UIGestureRecognizerStateChanged.");
            
        }else if(gr.state == UIGestureRecognizerStateChanged)
        {
            NSLog(@"UIGestureRecognizerStateChanged.");
            [self.curserView setHidden:NO];
            UITouch * touch = [touches anyObject];
            CGPoint newLocation = [touch locationInView:gr.view];
            CGPoint prevLocation = [touch previousLocationInView:gr.view];
            
            CGFloat distanceFromPrevious =fabs( prevLocation.x+newLocation.x);
            NSTimeInterval timeSincePrevious = event.timestamp - previousTimestamp;
            CGFloat speed = distanceFromPrevious/timeSincePrevious;
            previousTimestamp = event.timestamp;
            NSLog(@"dist %f | time %f | speed %f",distanceFromPrevious, timeSincePrevious, speed);
            
            if (newLocation.x > prevLocation.x) {
                //finger touch went right
                NSLog(@"right");
                if(right == 5 || speed >THRESHOLD_SPEED){
                    right = 0;
                    left = 0;
                    [self.textDocumentProxy adjustTextPositionByCharacterOffset:1];
                }else right++;
            } else if(newLocation.x < prevLocation.x){
                //finger touch went left
                if(left==5 || speed >THRESHOLD_SPEED){
                    left = 0;
                    right = 0;
                    NSLog(@"left");
                    [self.textDocumentProxy adjustTextPositionByCharacterOffset:-1];
                }else left++;
                
            }
            if (newLocation.y > prevLocation.y) {
                //finger touch went upwards
                NSLog(@"upwards");
            } else {
                //finger touch went downwards
                NSLog(@"downwards");
            }
            [self invalidOverlayTimer];
            overlayRemoveTimer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                                                  target:self
                                                                selector:@selector(overlayRemove:)
                                                                userInfo:nil
                                                                 repeats:NO];
            
        }
}
- (void) invalidOverlayTimer{
    [overlayRemoveTimer invalidate];
    overlayRemoveTimer  = nil;
}
- (void) overlayRemove:(id)sender{
    LongGesturePress = false;
    [self.curserView setHidden:YES];
}


#pragma mark - TextInput methods

- (void)textWillChange:(id<UITextInput>)textInput {
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)textDidChange:(id<UITextInput>)textInput {
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

#pragma mark - initialization method

- (void) initializeKeyboard {
    //NSLog(@"%s",__PRETTY_FUNCTION__);
    //Add guesture
    [self addGuestureOnKeyboard];
    //start with shift on
    _shiftStatus = 1;
    [self shiftKeys];
    
    //initialize space key double tap
    UITapGestureRecognizer *spaceDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(spaceKeyDoubleTapped:)];
    
    spaceDoubleTap.numberOfTapsRequired = 2;
    [spaceDoubleTap setDelaysTouchesEnded:NO];
    
    //[self.spaceButton addGestureRecognizer:spaceDoubleTap];
    
    //initialize shift key double and triple tap
    UITapGestureRecognizer *shiftDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shiftKeyDoubleTapped:)];
    UITapGestureRecognizer *shiftTripleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shiftKeyPressed:)];
    
    shiftDoubleTap.numberOfTapsRequired = 2;
    shiftTripleTap.numberOfTapsRequired = 3;
    
    [shiftDoubleTap setDelaysTouchesEnded:NO];
    [shiftTripleTap setDelaysTouchesEnded:NO];
    
    [self.shiftButton addGestureRecognizer:shiftDoubleTap];
    [self.shiftButton addGestureRecognizer:shiftTripleTap];
    
}

#pragma mark - key methods

- (IBAction) globeKeyPressed:(id)sender {
    //required functionality, switches to user's next keyboard
    [self advanceToNextInputMode];
}


- (IBAction) keyPressed:(UIButton*)sender {
    if(LongGesturePress == false){
        NSLog(@"%@",sender.titleLabel.text);
        [self animateButton:sender];
        //inserts the pressed character into the text document
        [self.textDocumentProxy insertText:sender.titleLabel.text];
        
        //if shiftStatus is 1, reset it to 0 by pressing the shift key
        if (_shiftStatus == 1) {
            [self shiftKeyPressed:self.shiftButton];
        }
    }
    
}

#pragma mark backspaceKeyPressed
-(IBAction) backspaceKeyPressed: (UIButton*) sender {
    [self.textDocumentProxy deleteBackward];
    
    UILongPressGestureRecognizer* longPressbackspaceKeyPressed = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleBackspaceKeyPressed:)];
    [longPressbackspaceKeyPressed setDelegate:self];
    [sender addGestureRecognizer:longPressbackspaceKeyPressed];
    
}
- (void)handleBackspaceKeyPressed:(UILongPressGestureRecognizer *)sender {
    NSLog(@" %s",__PRETTY_FUNCTION__);
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        NSLog(@"UIGestureRecognizerStateEnded");
        //Do Whatever You want on End of Gesture
        [backspaceTimer invalidate];
        backspaceTimer = nil;
    }
    else if (sender.state == UIGestureRecognizerStateBegan){
        NSLog(@"UIGestureRecognizerStateBegan.");
        //Do Whatever You want on Began of Gesture
        backspaceTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                          target:self
                                                        selector:@selector(deleteFunctionBackSpaceKeyPressed:)
                                                        userInfo:nil
                                                         repeats:YES];
        
    }
}
- (void) deleteFunctionBackSpaceKeyPressed:(id)sender{
    [self.textDocumentProxy deleteBackward];
}



-(IBAction) spaceKeyPressed: (UIButton*) sender {
    
    [self.textDocumentProxy insertText:@" "];
    
}


-(void) spaceKeyDoubleTapped: (UIButton*) sender {
    
    //double tapping the space key automatically inserts a period and a space
    //if necessary, activate the shift button
    [self.textDocumentProxy deleteBackward];
    [self.textDocumentProxy insertText:@". "];
    
    if (_shiftStatus == 0) {
        [self shiftKeyPressed:self.shiftButton];
    }
}


-(IBAction) returnKeyPressed: (UIButton*) sender {
    
    [self.textDocumentProxy insertText:@"\n"];
}


-(IBAction) shiftKeyPressed: (UIButton*) sender {
    
    //if shift is on or in caps lock mode, turn it off. Otherwise, turn it on
    _shiftStatus = _shiftStatus > 0 ? 0 : 1;
    
    [self shiftKeys];
}



-(void) shiftKeyDoubleTapped: (UIButton*) sender {
    
    //set shift to caps lock and set all letters to uppercase
    _shiftStatus = 2;
    
    [self shiftKeys];
    
}


- (void) shiftKeys {
    
    //if shift is off, set letters to lowercase, otherwise set them to uppercase
    if (_shiftStatus == 0) {
        for (UIButton* letterButton in self.letterButtonsArray) {
            [letterButton setTitle:letterButton.titleLabel.text.lowercaseString forState:UIControlStateNormal];
            [self addLongTouchGuestureView:letterButton];
        }
    } else {
        for (UIButton* letterButton in self.letterButtonsArray) {
            [letterButton setTitle:letterButton.titleLabel.text.uppercaseString forState:UIControlStateNormal];
            [self addLongTouchGuestureView:letterButton];
        }
    }
    
    //adjust the shift button images to match shift mode
    NSString *shiftButtonImageName = [NSString stringWithFormat:@"shift_%i.png", _shiftStatus];
    [self.shiftButton setImage:[UIImage imageNamed:shiftButtonImageName] forState:UIControlStateNormal];
    
    
    NSString *shiftButtonHLImageName = [NSString stringWithFormat:@"shift_%iHL.png", _shiftStatus];
    [self.shiftButton setImage:[UIImage imageNamed:shiftButtonHLImageName] forState:UIControlStateHighlighted];
    
}


- (IBAction) switchKeyboardMode:(UIButton*)sender {
    
    self.numbersRow1View.hidden = YES;
    self.numbersRow2View.hidden = YES;
    self.symbolsRow1View.hidden = YES;
    self.symbolsRow2View.hidden = YES;
    
    self.alphabatRow2View.hidden = YES;
    self.alphabatRow3View.hidden = YES;
    self.alphabatRow1View.hidden = YES;
    
    self.numbersSymbolsRow3View.hidden = YES;
    
    //switches keyboard to ABC, 123, or #+= mode
    //case 1 = 123 mode, case 2 = #+= mode
    //default case = ABC mode
    
    switch (sender.tag) {
            
        case 1: {
            
            self.numbersRow1View.hidden = NO;
            self.numbersRow2View.hidden = NO;
            self.numbersSymbolsRow3View.hidden = NO;
            
            //change row 3 switch button image to #+= and row 4 switch button to ABC
            [self.switchModeRow3Button setImage:[UIImage imageNamed:@"symbols.png"] forState:UIControlStateNormal];
            [self.switchModeRow3Button setImage:[UIImage imageNamed:@"symbolsHL.png"] forState:UIControlStateHighlighted];
            self.switchModeRow3Button.tag = 2;
            [self.switchModeRow4Button setImage:[UIImage imageNamed:@"abc.png"] forState:UIControlStateNormal];
            [self.switchModeRow4Button setImage:[UIImage imageNamed:@"abcHL.png"] forState:UIControlStateHighlighted];
            self.switchModeRow4Button.tag = 0;
        }
            break;
            
        case 2: {
            self.symbolsRow1View.hidden = NO;
            self.symbolsRow2View.hidden = NO;
            self.numbersSymbolsRow3View.hidden = NO;
            
            //change row 3 switch button image to 123
            [self.switchModeRow3Button setImage:[UIImage imageNamed:@"numbers.png"] forState:UIControlStateNormal];
            [self.switchModeRow3Button setImage:[UIImage imageNamed:@"numbersHL.png"] forState:UIControlStateHighlighted];
            self.switchModeRow3Button.tag = 1;
        }
            break;
            
        default:
            self.alphabatRow2View.hidden = NO;
            self.alphabatRow3View.hidden = NO;
            self.alphabatRow1View.hidden = NO;
            
            //change the row 4 switch button image to 123
            [self.switchModeRow4Button setImage:[UIImage imageNamed:@"numbers.png"] forState:UIControlStateNormal];
            [self.switchModeRow4Button setImage:[UIImage imageNamed:@"numbersHL.png"] forState:UIControlStateHighlighted];
            self.switchModeRow4Button.tag = 1;
            break;
    }
    
}

@end
