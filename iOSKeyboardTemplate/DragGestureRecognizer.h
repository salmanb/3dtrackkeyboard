//
//  DragGestureRecognizer.h
//  KeyboardTrackpad
//
//  Created by Munib Ahmed Siddiqui on 6/19/16.
//  Copyright © 2016 Jeff Blagg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIGestureRecognizerSubclass.h>

@interface DragGestureRecognizer : UILongPressGestureRecognizer

@property (nonatomic, retain) UITouch * firstTouch;
@property (nonatomic, retain) UITouch * lastTouch;

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end

@protocol DragGestureRecognizerDelegate <UIGestureRecognizerDelegate>
- (void) gestureRecognizer:(UIGestureRecognizer *)gr movedWithTouches:(NSSet*)touches andEvent:(UIEvent *)event;
@end