//
//  KeyboardViewController.h
//  iOSKeyboardTemplate
//
//  Copyright (c) 2014 BJH Studios. All rights reserved.
//  questions or comments contact jeff@bjhstudios.com

#import <UIKit/UIKit.h>
#import "DragGestureRecognizer.h"
#import <AudioToolbox/AudioServices.h>

@interface KeyboardViewController : UIInputViewController<DragGestureRecognizerDelegate>
{
    NSUInteger currentCurserPosition;
    NSTimer* backspaceTimer;
    NSTimer*  overlayRemoveTimer;
    BOOL LongGesturePress;
    int right,left;
    DragGestureRecognizer *newRecognizer;
    NSTimeInterval previousTimestamp;
    CFURLRef		soundFileURLRef;
    SystemSoundID	soundFileObject;
    
}
@property (readwrite)	CFURLRef		soundFileURLRef;
@property (readonly)	SystemSoundID	soundFileObject;

@end
